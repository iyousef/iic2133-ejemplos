#include "intbst.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#define N 200
#define C 10000

int print_tree(struct node *n,int id) {
  int this_id=id;
  int next_id=id+1;
  if (n->left) {
    printf("n%d -> n%d\n",this_id,next_id);
    next_id=print_tree(n->left,id+1);
  }
  if (n->right) {
    printf("n%d -> n%d\n",this_id,next_id);
    next_id=print_tree(n->right,next_id);
  }
  printf("n%d [label=\"%d\"]\n",this_id,n->element);
  return next_id;
}

void print_dot(struct node * n) {
  printf("digraph G {\n");
  print_tree(n,0);
  printf("}\n");
}

int uniform(int min, int max) {
  return ((double)rand()/(RAND_MAX+1.0)) * (max-min+1) + min;
}

int main() {

  int i;
  int A[N];
  int k;

  srand((unsigned) time(NULL));

  for (i=0; i<N;i++)
    A[i]=i+1;
  
  struct node *n=NULL;
  
  for (i=0; i<N;i++) {
    int j=uniform(0,N-1-i);
    n=insert(n,A[j]);
    for (;j<N-1-i;j++)
      A[j]=A[j+1];
  }
  print_dot(n);

  int min_depth[C];
  int max_depth[C];
  
  for (k=0; k<C; k++) {
    for (i=0; i<N;i++)
      A[i]=i+1;
    
    struct node *n=NULL;
    
    for (i=0; i<N;i++) {
      int j=uniform(0,N-1-i);
      n=insert(n,A[j]);
      for (;j<N-1-i;j++)
	A[j]=A[j+1];
    }
    min_depth[k]=minDepth(n);
    max_depth[k]=maxDepth(n);
  }

  double min_avg,max_avg;
  min_avg=max_avg=0;
  double max=0;
  double min=N*2;

  for (k=0;k<C;k++) {
    if (max<max_depth[k]) max=max_depth[k];
    if (min>min_depth[k]) min=min_depth[k];
    min_avg+=min_depth[k];
    max_avg+=max_depth[k];
  }
  min_avg/=C;
  max_avg/=C;
  
  fprintf(stderr,"height avg=%lf\n",max_avg);
  fprintf(stderr,"shortest branch avg=%lf\n",min_avg);
  fprintf(stderr,"max height=%lf\n",max);
  fprintf(stderr,"min shortest branch=%lf\n",min);
  fprintf(stderr,"log_2(%d)=%lf\n",N,log(N)/log(2));
  return 0;
}

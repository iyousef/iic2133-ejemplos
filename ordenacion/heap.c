#include <assert.h>
#include <stdlib.h>

#define left(x) (2*(x))
#define right(x) (2*(x)+1)
#define parent(x) ((x)/2)

static void heap_exchange(_HEAP_ *h,int i, int k) {
  _HEAP_ELEM_TYPE_ tmp;
  
  assert(i<=h->size && k<=h->size);
  
  tmp=h->A[i];
  h->A[i]=h->A[k];
  h->A[k]=tmp;
}

static void heap_sift_down(_HEAP_ *h, int i) {
  int l=left(i);
  int r=right(i);
  int largest=i;
  
  assert (i<=h->size);
    
  /* encontramos el elemento mayor entre el padre y ambos hijos */
  if (l <= h->size && _HEAP_KEY(h->A[l])>_HEAP_KEY(h->A[i]))
    largest=l;
  if (r <= h->size && _HEAP_KEY(h->A[r])>_HEAP_KEY(h->A[largest]))
    largest=r;
  
  if (largest!=i) {
    /* movemos el mayor a la raiz y seguimos recursivamente */
    heap_exchange(h,largest,i);
    heap_sift_down(h,largest);
  }
} 
 

static void heap_sift_up(_HEAP_ *h, int i) {
  assert(i<=h->size);
  while (i>1) {
    int p=parent(i);
    if (_HEAP_KEY(h->A[p])<=_HEAP_KEY(h->A[i])) 
      heap_exchange(h,i,p);
    i=p;
  }
}


void heap_insert(_HEAP_ *h, _HEAP_ELEM_TYPE_ elem) {
  h->A[++h->size]=elem; 
  heap_sift_up(h,h->size);
}
 
/* dado un arreglo cuyo primer elemento esta en la posición 1,
   reordena los elementos en A de tal manera que se cumpla la propiedad
   de heap. Además retorna una variable de tipo _HEAP_  */

_HEAP_ *build_heap_via_insert(_HEAP_ELEM_TYPE_ *A, int n) {
  int i;
  _HEAP_ *h = (_HEAP_ *)malloc(sizeof(_HEAP_));
  h->A=A;
  h->size=0;
  for (i=1;i<=n;i++) {
    heap_insert(h,A[i]);
  }
  return h;
}

_HEAP_ *build_heap(_HEAP_ELEM_TYPE_ *A, int n) {
  int i;
  _HEAP_ *h = (_HEAP_ *)malloc(sizeof(_HEAP_));
  h->A=A;
  h->size=n;
  for (i=n/2;i>=1;--i) {
    heap_sift_down(h,i);
  }
  return h;
}


void heap_sort(_HEAP_ELEM_TYPE_ *A, int n) {
  _HEAP_ *h=build_heap(A,n);
  int k;
  for (k=1;k<n;k++) {
    heap_exchange(h,1,h->size);
    --h->size;
    heap_sift_down(h,1);
  }
}

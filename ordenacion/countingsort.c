#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include <stdlib.h>
#include "config.h"
#include "utils.h"

int A[NUMBER];

int * countingsort(int A[],int n,int max) {
  
  int i;
  /* inizialización */
  int *C=(int*) malloc((max+1)*sizeof(int));
  int *D=(int*) malloc(n*sizeof(int));
  
  if (C==NULL || D==NULL) {
    printf("No se cuenta con la memoria suficiente.");
    exit(1);
  }
  for (i=0;i<=max;i++) C[i]=0;
  
  for (i=0;i<n;i++) {
    C[A[i]]++;
  }
  
  for (i=1;i<=max;i++) C[i]+=C[i-1];
  
  for (i=n-1;i>=0;i--) {
    assert(A[i]>=0);
    D[C[A[i]]-1]=A[i];
    --C[A[i]];
  }
  free(C);
  return D;
}


int main() {
  int k=0;
  int max=-1;
  
  assert(NUMBER<INT_MAX);

  k=read_array_max(A,&max);

  printf("max=%d\n",max);

  print_array(countingsort(A,k,max),k);

  return 0;

}

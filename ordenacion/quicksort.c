#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "config.h"
#include "utils.h"

static int partition(int A[],int p, int q) {
  int i,j;
  int x=A[q];
  i=p-1;
  for (j=p;j<q;++j) {
    if (A[j]<=x) {
      ++i;
      array_swap(A,i,j);
    }
  }
  array_swap(A,++i,q);
  return i;
}


void quicksort(int A[], int p, int r) {
  if (p<r) {
    int q=partition(A,p,r);
    quicksort(A,p,q-1);
    quicksort(A,q+1,r);
  }
}

int A[NUMBER];

int main() {
  int k=0;

  assert(NUMBER<INT_MAX);

  k=read_array(A);

  quicksort(A,0,k-1);

  print_array(A,k);

  return 0;

}

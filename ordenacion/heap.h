
typedef struct {
  _HEAP_ELEM_TYPE_ *A;
  int size;
} _HEAP_;

void heap_insert(_HEAP_ *h, _HEAP_ELEM_TYPE_ elem);
void heap_sort(_HEAP_ELEM_TYPE_ *A, int n);
_HEAP_ *build_heap(_HEAP_ELEM_TYPE_ *A, int n);

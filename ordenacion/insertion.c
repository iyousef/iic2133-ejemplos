#include <stdio.h>
#include <assert.h>
#include <limits.h>
#include "config.h"
#include "utils.h"

void insertion(int A[], int n) {
  int j;

  for (j=1; j<n; j++) {
    int key=A[j];
    int i=j-1;
    while (i>=0 && A[i]>key) {
      A[i+1]=A[i];
      i=i-1;
    }
    A[i+1]=key;
  }
}

int A[NUMBER];

int main() {
  int k=0;

  assert(NUMBER<INT_MAX);

  k=read_array(A);

  insertion(A,k);

  print_array(A,k);

  return 0;

}

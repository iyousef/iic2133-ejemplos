#include <stdio.h>
#include <stdlib.h>
 
#define MAX 10000000


int main(int argc, char *argv[]) {
  int c, n;
  
  if (argc!=2) {
    printf("Usage: %s positive_number\n",argv[0]);
    exit(1);
  }
  
  for (c = 0; c < atoi(argv[1]) ; c++) {
    n = rand()%MAX + 1;
    printf("%d\n", n);
  }
 
  return 0;
}

#include <stdio.h>
#include "config.h"
#include <string.h>

void array_swap(int A[], int i, int j) {
  int tmp=A[i];
  A[i]=A[j];
  A[j]=tmp;
}

int read_array_max(int A[], int *max) {
  int k=0;
  while (!feof(stdin) && k<NUMBER) {
    if (scanf("%d",&A[k])==1) {
      if (*max<A[k]) *max=A[k];
      k++;
    }
  }
  if (k==NUMBER) {
    printf("Limite de memoria excedido. Incremente NUMBER\n");
    exit(1);
  }
  return k;
}
 
int read_array(int A[]) {
  int k=0;
  while (!feof(stdin) && k<NUMBER) {
    if (scanf("%d",&A[k])==1)
      k++;
  }
  if (k==NUMBER) {
    printf("Limite de memoria excedido. Incremente NUMBER\n");
    exit(1);
  }
  return k;
}

int read_array_one(int A[]) {
  int k=1;
  while (!feof(stdin) && k<NUMBER) {
    if (scanf("%d",&A[k])==1)
      k++;
  }
  if (k==NUMBER) {
    printf("Limite de memoria excedido. Incremente NUMBER\n");
    exit(1);
  }
  return k-1;
}

void print_array(int A[], int N) {
  int i;
  for (i=0; i<N; i++)
    printf("%d\n",A[i]);

}

void print_array_one(int A[], int N) {
  int i;
  for (i=1; i<=N; i++)
    printf("%d\n",A[i]);

}
